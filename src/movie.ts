import express from "express";
import * as dotenv from 'dotenv';
import passport from 'passport';
import DB from './databaseController'
import EmbyConnector from 'emby-api-ts';

dotenv.config();

const movie = express.Router();
movie.use(passport.initialize());
movie.use(passport.session());

movie.get('/',passport.authenticate('jwt', {session: false}), async (req, res) => {
  const movies = await DB.Models.Movie.find()
  res.json(movies)
})

movie.put('/',passport.authenticate('jwt', {session: false}), updateMovies)

export = movie;

function updateMovies(res, req) {
  const emby = new EmbyConnector(process.env.EMBYURI, process.env.EMBYTOKEN)
  emby.getAllMovies().then((movies) =>{
    for (const item of movies.items){
      const movieItem = new DB.Models.Movie({
        title:    item.name,
        id:       item.id,
        serverId: item.serverId,
        keep:     false,
        tmdbID:   'lol',
        addedOn:  new Date(),
      });
      DB.Models.Movie.findOneAndUpdate({id: movieItem.id}, movieItem, {upsert: true}, function(err, doc) {
        if (err) return res.status(500).send(err);
      });
    }
  })
}
