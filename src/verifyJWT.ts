import jwt from 'jsonwebtoken';
import * as fs from 'fs';

const rsaPub = fs.readFileSync('./jwtRS256.key.pub')

export = function auth(req: any, res: any, next: any) {
  let token = req.header('Authorization');

  if (!token) return res.status(401).send({message: 'Access Denied'})

  if (!token.startsWith('Bearer ')) return res.status(401).send({message: 'Wrong Token'});

  token = token.slice(7, token.length);
  jwt.verify(token, rsaPub, (err: any, decoded: any) => {
    if (err) return res.status(401).send({message: 'Wrong Token'});
    req.user = decoded.sub;
    next();
  });
}
