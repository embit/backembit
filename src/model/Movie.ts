import mongoose, { Schema } from 'mongoose';

export interface IMovie extends mongoose.Document {
  title: string;
  id: string;
  serverId: string;
  keep: boolean;
  tmdbID: string;
  addedOn: Date;
}

export interface MovieModel extends mongoose.Model<IMovie> {};

export class Movie {
  private _model: MovieModel;

  constructor(){
    const schema = new Schema({
      title:    {type: String, required: true},
      id:       {type: String, required: true, unique: true},
      serverId: {type: String, required: true},
      keep:     {type: Boolean},
      tmdbID:   {type: String},
      addedOn:  {type: Date},
    }, {timestamps: true});
    this._model = mongoose.model<IMovie>('Movie', schema);
  }

  public get model(): MovieModel{
    return this._model;
  }
}
