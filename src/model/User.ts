import mongoose, { Schema } from 'mongoose';

export interface IUser extends mongoose.Document {
  login: string;
  password: string;
  accessToken?: string;
  serverId?: string;
  embyId?: string;
}

export interface UserModel extends mongoose.Model<IUser> {};

export class User {

  private _model: UserModel;

  constructor(){
    const schema = new Schema({
      login: { type: String, unique: true, required: true},
      password: { type: String, required: true },
      accessToken: {type: String},
      serverId: {type: String},
      embyId: {type: String, unique: true}
    }, {timestamps: true});
    this._model = mongoose.model<IUser>('User', schema);
  }

  public get model(): UserModel {
    return this._model;
  }
}

