import express from 'express';
import mongoose from 'mongoose';
import embit from './embit';
import movie from './movie';
import cors from 'cors';
import * as dotenv from 'dotenv';

dotenv.config();
const app = express();
const port = process.env.EX_PORT;

app.use(cors());
app.use(express.json());
app.use('/embit', embit);
app.use('/movie', movie);

app.listen(port, () => {
  console.log(`Now listening on port: ${port}`);
});

