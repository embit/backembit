import express from "express";
import bcrypt from 'bcryptjs';
import uuid from 'uuid';
import jwt from 'jsonwebtoken';
import * as fs from 'fs';
import * as dotenv from 'dotenv';
import {ExtractJwt, Strategy} from 'passport-jwt';
import passport from 'passport';
import DB from './databaseController'
import EmbyConnector from 'emby-api-ts';

dotenv.config();

const embit = express.Router();
embit.use(passport.initialize());
embit.use(passport.session());

const refreshTokens: { [key: string]: string;} = {};

const rsaPriv = fs.readFileSync(process.env.PRIV_FILE);
const rsaPub = fs.readFileSync(process.env.PUB_FILE);

const passportOpts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey : rsaPub,
}

passport.use(new Strategy(passportOpts, (JWTPayload, callback) =>{
  const expDate = new Date(JWTPayload.exp * 1000);
  if (expDate < new Date())
    return callback(null, false);
  callback(null, JWTPayload);
}));

passport.serializeUser((user: {_id: string}, callback) => {
  callback(null, user._id);
});

embit.post('/register', (req, res) => {
  const emby = new EmbyConnector(process.env.EMBYURI)
  emby.authenticateByName(req.body.login, req.body.password).then((data) =>{
    bcrypt.genSalt(12, (err: Error, salt: string) => {
      if (err) return res.send(err);
      bcrypt.hash(req.body.password, salt).then((hashPassword) => {
        const user = new DB.Models.User({
          login: req.body.login,
          password: hashPassword,
          accessToken: data.accessToken,
          serverId: data.serverId,
          embyId: data.user.id
        });
        user.save().then(() => {
          res.json({id: user._id});
        }, (errMongo) => { res.status(500).send(errMongo); });
      }, (errBcrypt) => { res.status(500).send(errBcrypt); });
    });
  }, (errEmby) => {res.status(500).send(errEmby)});
});

embit.post('/login', (req, res) => {
  DB.Models.User.findOne({login: req.body.login}, (errMongo, user) => {
    if (errMongo) return res.status(500).send(errMongo);
    if (!user) return res.status(401).json({message: 'password or login is wrong'});
    bcrypt.compare(req.body.password, user.password, (errBcrypt, success) => {
      if (errBcrypt) return res.status(500).send(errBcrypt);
      if (success) {
        const emby = new EmbyConnector(process.env.EMBYURI)
        emby.authenticateByName(req.body.login, req.body.password).then((data) => {
          DB.Models.User.updateOne({login: req.body.login}, {accessToken: data.accessToken}, (errMongoUpdate, userUpdate) => {
            if (errMongoUpdate) return res.status(500).send(errMongoUpdate);
            return userUpdate;
          });
        });
        const token = jwt.sign({}, rsaPriv, {
          algorithm: 'RS256',
          expiresIn: 600,
          subject: user._id.toString(),
        });
        const refreshToken = uuid.v4();
        refreshTokens[refreshToken] = user._id;
        return res.json({id: user._id, refresh: refreshToken, jwt: token});
      }
      else return res.status(401).json({message: 'password or login is wrong'});
    })
  })
});

embit.post('/logout', (req, res) =>{
  const refreshToken = req.body.refreshToken;
  if (refreshToken in refreshTokens) {
    delete refreshTokens[refreshToken];
  }
  res.status(204).json({message: 'User logged out'});
});

embit.post('/refresh', (req, res) => {
  const refreshToken = req.body.refreshToken;
  if (refreshToken in refreshTokens) {
    const _id = refreshTokens[refreshToken];
    const token = jwt.sign({}, rsaPriv, {
      algorithm: 'RS256',
      expiresIn: 600,
      subject: _id.toString(),
    });
    return res.json({id: _id, refresh: refreshToken, jwt: token});
  }
  else {
    res.status(400).json({message: 'Invalid refresh token'});
  }
});

export = embit;
