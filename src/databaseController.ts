import { connect, connection, Connection } from 'mongoose';
import mongoose from 'mongoose';
import { User, UserModel } from './model/User';
import { Movie, MovieModel } from './model/Movie';

declare interface IModels {
    User: UserModel;
    Movie: MovieModel;
}

export default class DB {

    private static instance: DB;
    private _db: Connection;
    private _models: IModels;

    private constructor() {
      mongoose.set('useFindAndModify', false);
      mongoose.set('useCreateIndex', true);
      mongoose.set('useNewUrlParser', true);
      mongoose.set('useUnifiedTopology', true);
      const uri = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
        connect(uri);
      this._db = connection;
      this._db.on('open', this.connected);
      this._db.on('error', this.error);

      this._models = {
        User: new User().model,
        Movie: new Movie().model
      }
    }

    public static get Models() {
      if (!DB.instance) {
        DB.instance = new DB();
      }
      return DB.instance._models;
    }

    private connected() {
      console.log('Mongoose has connected');
    }

    private error(error: any) {
      console.error('Mongoose has errored', error);
    }
}
